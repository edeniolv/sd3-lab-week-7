package com.example.api.controller;

import com.example.api.model.entity.User;
import com.example.api.model.service.JwtService;
import com.example.api.model.service.UserService;
import com.example.api.utils.constants.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;
    private final JwtService jwtService;

    @Autowired
    public UserController(UserService userService, JwtService jwtService) {
        this.userService = userService;
        this.jwtService = jwtService;
    }

    @PostMapping("/register")
    public ResponseEntity<Object> register(@RequestBody User user) {
        User newUser = userService.create(user);
        
        if (newUser == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Not created User");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(user);
    }
    
    @PostMapping("/login")
    public ResponseEntity<Object> login(Authentication auth) {
        String token = jwtService.encode(auth).getTokenValue();
        return ResponseEntity.status(HttpStatus.OK).body(token);
    }
    
    @GetMapping
    public ResponseEntity<Object> readAll() {
        Iterable<User> users = userService.readAll();
        
        if (!users.iterator().hasNext()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not contains Users");
        }

        return ResponseEntity.status(HttpStatus.OK).body(users);
    }
    
    @GetMapping("/id/{id}")
    public ResponseEntity<Object> readById(@PathVariable UUID id) {
        User user = userService.readById(id);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found by ID");
        }

        return ResponseEntity.status(HttpStatus.OK).body(user);
    }

    @GetMapping("/username/{username}")
    public ResponseEntity<Object> readByUsername(@PathVariable String username) {
        User user = userService.readByUsername(username);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found by Username");
        }

        return ResponseEntity.status(HttpStatus.OK).body(user);
    }
    
    @GetMapping("/role/{role}")
    public ResponseEntity<Object> readByRole(@PathVariable Role role) {
        Iterable<User> users = userService.readByPermission(role);

        if (users == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found by ID");
        }

        return ResponseEntity.status(HttpStatus.OK).body(users);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@PathVariable UUID id, @RequestBody User user) {
        User updatedUser = userService.update(id, user);

        if (updatedUser == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Not updated User");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(updatedUser);
    }
    
    @PatchMapping("/{id}/username")
    public ResponseEntity<Object> updateUsername(@PathVariable UUID id, @RequestBody Map<String, String> body) {
        User updatedUser = userService.updateUsername(id, body.get("username"));

        if (updatedUser == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Username not updated");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(updatedUser);
    }

    @PatchMapping("/{id}/password")
    public ResponseEntity<Object> updatePassword(@PathVariable UUID id, @RequestBody Map<String, String> body) {
        User updatedUser = userService.updatePassword(id, body.get("password"));

        if (updatedUser == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Password not updated");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(updatedUser);
    }
    
    @PatchMapping("/{id}/role")
    public ResponseEntity<Object> updatePermission(@PathVariable UUID id, @RequestBody Map<String, Role> body) {
        User updatedUser = userService.updatePermission(id, body.get("role"));

        if (updatedUser == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Permission not updated");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(updatedUser);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> remove(@PathVariable UUID id) {
        if (userService.removeById(id) <= 0) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Not deleted User");
        }
        
        return ResponseEntity.status(HttpStatus.OK).body("Deleted User");
    }
}
