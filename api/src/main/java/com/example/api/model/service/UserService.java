package com.example.api.model.service;

import com.example.api.model.entity.User;
import com.example.api.model.repository.UserRepository;
import com.example.api.utils.constants.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository repository;
    private final PasswordEncoder passwdEncoder;

    @Autowired
    public UserService(UserRepository repository, PasswordEncoder passwdEncoder) {
        this.repository = repository;
        this.passwdEncoder = passwdEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findUserByUsername(username);
        return Optional.of(user).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }
    
    public User create(User user) {
        User oldUser = repository.findUserByUsername(user.getUsername());
        
        if (oldUser != null) {
            return null;
        }

        user.setPassword(getEncryptPassword(user.getPassword()));
        return repository.save(user);
    }
    
    public Iterable<User> readAll() {
        return repository.findAll();
    }
    
    public User readByUsername(String username) {
        if (username.isEmpty()) {
            return null;
        }

        return repository.findUserByUsername(username);
    }
    
    public User readById(UUID id) {
        if (id.toString().isEmpty()) {
            return null;
        }
        
        return repository.findUserByIdEquals(id);
    }
    
    public Iterable<User> readByPermission(Role role) {
        if (role == null) {
            return null;
        }
        
        return repository.findUsersByRoleEquals(role);
    }
    
    public User update(UUID id, User user) {
        User oldUser = repository.findUserByIdEquals(id);
        
        if (oldUser == null) {
            return null;
        }
        
        oldUser.setUsername(user.getUsername());
        oldUser.setPassword(getEncryptPassword(user.getPassword()));
        oldUser.setRole(user.getRole());
        
        return repository.save(oldUser);
    }
    
    public User updateUsername(UUID id, String username) {
        User oldUser = repository.findUserByIdEquals(id);

        if (oldUser == null) {
            return null;
        }

        oldUser.setUsername(username);
        return repository.save(oldUser);
    }

    public User updatePassword(UUID id, String password) {
        User oldUser = repository.findUserByIdEquals(id);

        if (oldUser == null) {
            return null;
        }

        oldUser.setPassword(getEncryptPassword(password));
        return repository.save(oldUser);
    }
    
    public User updatePermission(UUID id, Role role) {
        User oldUser = repository.findUserByIdEquals(id);

        if (oldUser == null) {
            return null;
        }

        oldUser.setRole(role);
        return repository.save(oldUser);
    }
    
    public Integer removeById(UUID id) {
        return repository.removeUserByIdEquals(id);
    }
    
    private String getEncryptPassword(String password) {
        return passwdEncoder.encode(password);
    }
}
