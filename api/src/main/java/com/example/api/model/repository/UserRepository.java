package com.example.api.model.repository;

import com.example.api.model.entity.User;
import com.example.api.utils.constants.Role;
import jakarta.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface UserRepository extends CrudRepository<User, UUID> {
    User findUserByUsername(String username);
    User findUserByIdEquals(UUID id);
    Iterable<User> findUsersByRoleEquals(Role role);
    @Transactional
    Integer removeUserByIdEquals(UUID id);
}
