package com.example.api.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class JwtService {
    private final JwtEncoder encoder;
    private static final Instant CURRENT = Instant.now();
    private static final long EXPIRY = 3600L;

    @Autowired
    public JwtService(JwtEncoder encoder) {
        this.encoder = encoder;
    }
    
    public Jwt encode(Authentication auth) {
        String scopes = getScopes(auth);
        JwtClaimsSet claims = getClaims(auth, scopes);
        return encoder.encode(JwtEncoderParameters.from(claims));
    }
    
    private String getScopes(Authentication auth) {
        return auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(" "));
    }
    
    private JwtClaimsSet getClaims(Authentication auth, String scopes) {
        JwtClaimsSet.Builder claims = JwtClaimsSet.builder();
        claims.issuer("spring-security=swt");
        claims.issuedAt(Instant.now());
        claims.expiresAt(CURRENT.plusSeconds(EXPIRY));
        claims.subject(auth.getName());
        claims.audience(List.of("api-cis"));
        claims.claim("scope", scopes);
        return claims.build();
    }
}
