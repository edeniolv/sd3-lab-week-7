package com.example.api.utils.security;

import com.example.api.utils.constants.Authority;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.proc.SecurityContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.web.SecurityFilterChain;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Value("${jwt.public.key}")
    private RSAPublicKey publicKey;

    @Value("${jwt.private.key}")
    private RSAPrivateKey privateKey;
    
    @Bean
    protected SecurityFilterChain filter(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable);
        http.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.authorizeHttpRequests(auth -> {
            auth.requestMatchers(HttpMethod.POST, "/api/users/register").permitAll();
            auth.requestMatchers(HttpMethod.POST, "/api/users/login").hasAuthority(Authority.ROLE_LOGIN.getType());
            auth.requestMatchers(HttpMethod.GET, "/api/users").hasAuthority(Authority.ROLE_ADMIN.getType());
            auth.requestMatchers(HttpMethod.GET, "/api/users/id/{id}").hasAuthority(Authority.ROLE_USER.getType());
            auth.requestMatchers(HttpMethod.GET, "/api/users/username/{username}").hasAuthority(Authority.ROLE_USER.getType());
            auth.requestMatchers(HttpMethod.GET, "/api/users/role/{role}").hasAuthority(Authority.ROLE_ADMIN.getType());
            auth.requestMatchers(HttpMethod.PUT, "/api/users/{id}").hasAuthority(Authority.ROLE_ADMIN.getType());
            auth.requestMatchers(HttpMethod.PATCH, "/api/users/{id}/username").hasAuthority(Authority.ROLE_USER.getType());
            auth.requestMatchers(HttpMethod.PATCH, "/api/users/{id}/password").hasAuthority(Authority.ROLE_USER.getType());
            auth.requestMatchers(HttpMethod.PATCH, "/api/users/{id}/role").hasAuthority(Authority.ROLE_ADMIN.getType());
            auth.requestMatchers(HttpMethod.DELETE, "/api/users/{id}").hasAuthority(Authority.ROLE_USER.getType());
            auth.anyRequest().authenticated(); 
        });
        http.httpBasic(Customizer.withDefaults());
        http.oauth2ResourceServer(config -> config.jwt(Customizer.withDefaults()));
        return http.build();
    }
    
    @Bean
    protected JwtEncoder encoderJwt() {
        RSAKey key = new RSAKey.Builder(publicKey).privateKey(privateKey).build();
        JWKSet jwk = new JWKSet(key);
        ImmutableJWKSet<SecurityContext> jwks = new ImmutableJWKSet<>(jwk);
        return new NimbusJwtEncoder(jwks);
    }
    
    @Bean
    protected JwtDecoder decoderJwt() {
        return NimbusJwtDecoder.withPublicKey(publicKey).build();
    }
    
    @Bean
    protected PasswordEncoder encryptPassword() {
        return new BCryptPasswordEncoder();
    }
}
