package com.example.api.utils.constants;

import lombok.Getter;

@Getter
public enum Authority {
    ROLE_ADMIN("SCOPE_ROLE_ADMIN"),
    ROLE_USER("SCOPE_ROLE_USER"),
    ROLE_LOGIN("ROLE_USER");
    
    private final String type;

    Authority(String type) {
        this.type = type;
    }
}
