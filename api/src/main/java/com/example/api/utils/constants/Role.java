package com.example.api.utils.constants;

public enum Role {
    ADMIN, USER
}
